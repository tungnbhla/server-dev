const express = require('express')
const morgan = require('morgan')
const bodyParser = require('body-parser');
var cors = require('cors')
const app = express()
const port = process.env.PORT || 3001

//connect db
const db = require('./config/db');
db.connect();

app.use(bodyParser.json());

app.use(morgan('combined'));


// app.post('/register', async (req, res) => {
//   User.find({})
//   try {
//     const { username, email, password } = req.body;
//     const user = new User({ username, email, password });
//     await user.save();
//     res.status(201).json({ message: 'Sign Up Success' });
//   } catch (error) {
//     res.status(500).json({ message: 'An error occurred while registering the user' });
//   }
// });


// app.post('/login', async (req, res) => {
//   try {
//     const { email, password } = req.body;
//     const user = await User.findOne({ email: email });

//     if (!user) {
//       return res.status(401).json({ message: 'Email không tồn tại' });
//     }

//     // Kiểm tra mật khẩu
//     if (user.password !== password) {
//       return res.status(401).json({ message: 'Mật khẩu không chính xác' });
//     }

//     res.status(200).json({ message: 'Đăng nhập thành công', user: user });
//   } catch (error) {
//     res.status(500).json({ message: 'Đã xảy ra lỗi khi đăng nhập' });
//   }
// });

// Page Home
app.get("/", (req, res) => {
  res.send('SERVER ON')
})

// ZingMp3Router
const ZingMp3Router = require("./app/router/ZingRouter")
app.use("/api", cors({ origin: '*' }), ZingMp3Router)

// CousersRouter
const CousersRouter = require("./app/router/CousersRouter")
app.use("/beta", cors({ origin: '*' }), CousersRouter)

// Page Error
app.get("*", (req, res) => {
    res.send("Input Wrong Path! Please Enter Again >.<")
});

app.use(cors())
 app.listen(port,() => {
  console.log(`Example app listening on port ${port}`)
})