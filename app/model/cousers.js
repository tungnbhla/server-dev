const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Couser = new Schema({
  name: {type: String, maxLength: 255},
  description: {type: String, maxLength: 600},
  img: {type: String, maxLength: 600},
  createdAt: {type: Date, default: Date.now},
  updatedAt: {type: Date, default: Date.now},
});

module.exports = mongoose.model('Couser', Couser);