const Couser = require('../model/cousers');

class CousersController {

    getCousers(req, res) {
        Couser.find({})
            .then(cousers => {
                res.json(cousers);
            })
            .catch(err => {
                res.status(400).json({ error: "Error..." });
            });
    }
}

module.exports = new CousersController